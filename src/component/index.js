import Header from './Header'
import Status from './Status'
import Content from './Content'
import C1 from './C1'
import C2 from './C2'
import C3 from './C3'
import C4 from './C4'
import C5 from './C5'
import C6 from './C6'
import C7 from './C7'


export {Header,Status,Content,C1,C2,C3,C4,C5,C6,C7}