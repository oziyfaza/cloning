import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import {Header,Status,Content,C1,C2,C3,C4,C5,C6,C7} from './index'


const App = () => {
  return (
    <View>
      <Header />
      <ScrollView>
        <Status />
        <Content />
        <C1/>
        <C2/>
        <C3/>
        <C4/>
        <C5/>
        <C6/>
        <C7/>

      </ScrollView>
    </View>
  )
}

export default App
