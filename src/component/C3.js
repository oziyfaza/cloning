import React from 'react'
import {View, Image,Text,StyleSheet,TouchableOpacity} from 'react-native'
import  Icon  from 'react-native-vector-icons/Feather'
import  Icon1  from 'react-native-vector-icons/Ionicons'
import  Icon2  from 'react-native-vector-icons/Ionicons'
import  Icon3  from 'react-native-vector-icons/Ionicons'
import { useState } from 'react'



const Halaman = () => {
    const [nomor, setNumber] = useState(0)
    return (
        <View>
            <View style={style.logo1}>
            <Image style={{width:35,height:35,marginRight:30,marginTop:10,borderRadius:20,marginVertical:10,marginHorizontal:10}} source={require('../assets/cowok2.jpg')}/>
            <Text style={{fontWeight:'bold',marginVertical:18,minWidth:30,}}>PUNCAK INDAH</Text>
            <Icon3 style={style.Icon3} color="yellow" name="ellipsis-vertical" size={25}/>
            </View>
            <Image source={require('../assets/3.jpg')} style={{height:260,width:360}}/>
            <View style={style.logo1}>
            <TouchableOpacity>
            <Icon style={style.navBawah} color="yellow" name="heart" size={30} onPress={() => setNumber(nomor + 1)}/>
            </TouchableOpacity>
            <Icon1 style={style.navItem} color="yellow" name="chatbubble-ellipses" size={30}/>
            <Icon2 style={style.navItem} color="yellow" name="paper-plane" size={30}/>
            <Icon3 style={style.logo4} color="yellow" name="bookmarks" size={30}/>
            </View>
            <View style={style.text}>
            <Text style={{fontWeight:'bold',marginHorizontal:5}}>{nomor} Suka</Text>
            <Text style={{fontSize:12,fontWeight: '900',marginHorizontal:5}}>Sebuah pemandangan alam di pegunungan akan membuat hati kita nyaman karena apa yang kita lihat tidak terlalu membuat pikiran kita berpikir keras, tapi malah akan membuat hati dan pikiran kita nyaman.</Text>
            </View>
            </View>


    )
}
   
const style= StyleSheet. create({
    Icon3: {
        flexDirection: 'row-reverse',
        marginHorizontal:150,
        marginVertical:10
    },
    logo1: {
      flexDirection:'row',
        backgroundColor:'white',
        elevation:1
    },
    navItem: {
        marginHorizontal:10,
        marginVertical:8
    },
    logo4: {
        marginHorizontal:165,
        marginVertical:8
    },
    text: {
        height:100,
        backgroundColor:'white',
        flexDirection:'column',
    },
    logo3: {
        height:55,
        flexDirection:'row',
        backgroundColor:'white',
    },
    navBawah: {
        marginHorizontal:10,
        marginVertical:8
    },
    baca: {
        height:100,
        backgroundColor:'white',
        flexDirection:'column',
    }
});

export default Halaman
